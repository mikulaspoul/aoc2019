import sys
from pathlib import Path

import pytest


def load_file(text=None):
    if text is None:
        text = Path("input.txt").read_text()
    return [int(x) for x in text.split(",")]


def get_input_and_output_location(memory, current_index):
    a_loc = memory[current_index + 1]
    b_loc = memory[current_index + 2]
    output_loc = memory[current_index + 3]

    return memory[a_loc], memory[b_loc], output_loc


def process_memory(memory):
    current_index = 0
    while True:
        operand = memory[current_index]
        if operand not in {1, 2, 99}:
            raise ValueError("Invalid operand")
        if operand == 99:
            break

        a, b, output_loc = get_input_and_output_location(memory, current_index)

        if operand == 1:
            output = a + b
        else:
            output = a * b

        memory[output_loc] = output

        current_index += 4

    return memory


@pytest.mark.parametrize(
    "memory,output",
    [
        ["1,9,10,3,2,3,11,0,99,30,40,50", "3500,9,10,70,2,3,11,0,99,30,40,50"],
        ["1,0,0,0,99", "2,0,0,0,99"],
        ["2,3,0,3,99", "2,3,0,6,99"],
        ["2,4,4,5,99,0", "2,4,4,5,99,9801"],
        ["1,1,1,4,99,5,6,0,99", "30,1,1,4,2,5,6,0,99"],
    ],
)
def test_process_memory(memory, output):
    memory = load_file(memory)

    assert load_file(output) == process_memory(memory)


def get_output_with_inputs(noun, verb):
    main_memory = load_file()

    main_memory[1] = noun
    main_memory[2] = verb

    main_memory = process_memory(main_memory)

    return main_memory[0]


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print(get_output_with_inputs(12, 2))
    else:
        desired_output = 19690720

        for noun in range(0, 100):
            for verb in range(0, 100):
                if get_output_with_inputs(noun, verb) == desired_output:
                    print(100 * noun + verb)
