import pytest


def valid_password(password):
    if len(password) != 6:
        return False

    for i in range(5):
        if int(password[i]) > int(password[i + 1]):
            return False

    doubles = set()
    for x in range(0, 10):
        if f"{x}{x}" in password:
            doubles.add(x)
    if not doubles:
        return False

    for double in list(doubles):
        if f"{double}{double}{double}" in password:
            doubles.discard(double)

    if not doubles:
        return False

    return True


@pytest.mark.parametrize(
    ["x", "valid"],
    [
        ("111111", False),
        ("223450", False),
        ("123789", False),
        ("112233", True),
        ("123444", False),
        ("111122", True),
    ],
)
def test_valid_password(x, valid):
    assert valid == valid_password(x)


if __name__ == "__main__":
    count = 0
    for p in range(171309, 643604):
        if valid_password(str(p)):
            count += 1

    print(count)
