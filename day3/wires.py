from pathlib import Path
from pprint import pprint
from typing import Tuple, Dict

import pytest
import numpy as np
from scipy.spatial.distance import cityblock


def parse_wire(wire):
    path = []

    for x in wire.split(","):
        direction, distance = x[0], int(x[1:])

        if direction == "R":
            path.append((distance, 0))
        elif direction == "D":
            path.append((0, -distance))
        elif direction == "L":
            path.append((-distance, 0))
        elif direction == "U":
            path.append((0, distance))
        else:
            raise ValueError

    return path


def get_axis(path, current_loc):
    if path == 0:
        return current_loc
    else:
        if path > 0:
            return slice(current_loc + 1, current_loc + path + 1)
        else:
            return slice(current_loc + path, current_loc)


def s(x):
    if isinstance(x, slice):
        return f"{x.start}:{x.stop}"
    return x


def draw(starting_point, wire, field, by=1):
    current_loc = list(starting_point)
    speed: Dict[Tuple[int, int], int] = {}

    step = 0

    for path in wire:
        x_axis = get_axis(path[0], current_loc[0])
        y_axis = get_axis(path[1], current_loc[1])

        # print(s(x_axis), s(y_axis))

        field[y_axis, x_axis] += by

        if isinstance(y_axis, int):
            r = range(x_axis.start, x_axis.stop)
            if path[0] < 0:
                r = reversed(r)
            for x in r:
                step += 1
                speed.setdefault((y_axis, x), step)
        else:
            r = range(y_axis.start, y_axis.stop)
            if path[1] < 0:
                r = reversed(r)
            for y in r:
                step += 1
                speed.setdefault((y, x_axis), step)

        current_loc[0] += path[0]
        current_loc[1] += path[1]

    return speed


def nearest_intersection(wires):
    wire_one, wire_two = wires.strip().split("\n")

    wire_one = parse_wire(wire_one)
    wire_two = parse_wire(wire_two)

    field = np.zeros((50_000, 50_000))
    starting_point = (25_000, 25_000)

    speed_one = draw(starting_point, wire_one, field, by=2)
    speed_two = draw(starting_point, wire_two, field, by=3)

    nearest = 2 ** 31
    fastest = 2 ** 31

    for index in np.argwhere(field == 5):
        distance = cityblock(starting_point, index)

        if distance == 0:  # is the starting point
            continue

        # print(distance)

        if distance < nearest:
            nearest = distance

        index = (index[0], index[1])
        combined_speed = speed_one[index] + speed_two[index]
        if combined_speed < fastest:
            fastest = combined_speed

    return nearest, fastest


@pytest.mark.parametrize(
    ["wires", "distance", "fastest"],
    [
        ["R8,U5,L5,D3\nU7,R6,D4,L4", 6, 30],
        [
            "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83",
            159,
            610,
        ],
        [
            "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7",
            135,
            410,
        ],
    ],
)
def test_wires(wires, distance, fastest):
    assert nearest_intersection(wires) == (distance, fastest)


def test_draw():
    array = np.zeros((20, 20), dtype=int)
    one_steps = draw((10, 10), parse_wire("R8,U5,L5,D3"), array)
    two_steps = draw((10, 10), parse_wire("U7,R6,D4,L4"), array)

    correct = {
        (10, 11): 1,
        (10, 12): 2,
        (10, 13): 3,
        (10, 14): 4,
        (10, 15): 5,
        (10, 16): 6,
        (10, 17): 7,
        (10, 18): 8,
        (11, 18): 9,
        (12, 18): 10,
        (13, 18): 11,
        (14, 18): 12,
        (15, 18): 13,
        (15, 17): 14,
        (15, 16): 15,
        (15, 15): 16,
        (15, 14): 17,
        (15, 13): 18,
        (14, 13): 19,
        (13, 13): 20,
        (12, 13): 21
    }
    print(one_steps)

    assert one_steps == correct

    # assert two_steps is None

    print(np.flip(array, 0))
    x = np.array(
        [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 2, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 2, 1, 1, 1, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ]
    )

    assert (np.flip(array, 0) == x).all()


if __name__ == "__main__":
    # array = np.zeros((20, 20), dtype=int)
    # print("R8,U5,L5,D3")
    # draw((10, 10), parse_wire("R8,U5,L5,D3"), array)
    # print("U7,R6,D4,L4")
    # draw((10, 10), parse_wire("U7,R6,D4,L4"), array)
    #
    # print(np.flip(array, 0))
    #
    # x = "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83"
    # array = np.zeros((500, 500), dtype=int)
    # draw((250, 250), parse_wire(x.split("\n")[0]), array, by=2)
    # draw((250, 250), parse_wire(x.split("\n")[1]), array, by=3)
    # plt.imshow(array, cmap="hot", interpolation="nearest")
    # plt.show()

    print(nearest_intersection(Path("input.txt").read_text()))
