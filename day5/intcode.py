from pathlib import Path

import pytest


def load_file(text=None):
    if text is None:
        text = Path("input.txt").read_text()
    return [int(x) for x in text.split(",")]


def get_value(memory, mode, param):
    if mode == 0:
        return memory[param]
    return param


def less_than(a, b):
    return a < b


def equals(a, b):
    return a == b


def process_memory(memory, input_value=1):
    outputs = []

    current_index = 0
    while True:
        instruction = memory[current_index]
        str_instruction = str(instruction)
        operand = int(str_instruction[-2:])

        try:
            a_mode = int(str_instruction[-3])
        except IndexError:
            a_mode = 0
        try:
            b_mode = int(str_instruction[-4])
        except IndexError:
            b_mode = 0

        if operand not in {1, 2, 3, 4, 5, 6, 7, 8, 99}:
            raise ValueError(f"Invalid operand {operand}")
        if operand == 99:
            break

        if operand in {1, 2}:
            a_loc = memory[current_index + 1]
            b_loc = memory[current_index + 2]
            output_loc = memory[current_index + 3]

            a = get_value(memory, a_mode, a_loc)
            b = get_value(memory, b_mode, b_loc)

            if operand == 1:
                output = a + b
            else:
                output = a * b

            print(memory[current_index : current_index + 4], a, b, output, output_loc)

            memory[output_loc] = output

        elif operand == 3:
            output_loc = memory[current_index + 1]
            memory[output_loc] = input_value

            print(memory[current_index : current_index + 2], f"set {output_loc} to {input_value}")
        elif operand == 4:
            output_loc = memory[current_index + 1]
            output = get_value(memory, a_mode, output_loc)

            outputs.append(output)

            print(memory[current_index : current_index + 2], f"output is {output}")

        elif operand in {5, 6}:
            condition_loc = memory[current_index + 1]
            cond = int(operand == 5)

            value = get_value(memory, a_mode, condition_loc)

            if int(value > 0) == cond:
                current_index = get_value(memory, b_mode, memory[current_index + 2])
                print(memory[current_index : current_index + 3], value, cond, f"jumping to {current_index}")
                continue
            else:
                print(memory[current_index: current_index + 3], value, cond, f"not jumping")

        elif operand in {7, 8}:
            a_loc = memory[current_index + 1]
            b_loc = memory[current_index + 2]
            output_loc = memory[current_index + 3]

            a = get_value(memory, a_mode, a_loc)
            b = get_value(memory, b_mode, b_loc)

            comparison = less_than if operand == 7 else equals

            memory[output_loc] = int(comparison(a, b))

            print(memory[current_index : current_index + 3], a, b, memory[output_loc])

        if operand in {1, 2, 7, 8}:
            current_index += 4
        elif operand in {5, 6}:
            current_index += 3
        else:
            current_index += 2

    return memory, outputs


@pytest.mark.parametrize("program", [
    "3,9,8,9,10,9,4,9,99,-1,8",
    "3,3,1108,-1,8,3,4,3,99",
])
def test_equal(program):
    _, outputs = process_memory(load_file(program), input_value=8)
    assert outputs[0] == 1

    _, outputs = process_memory(load_file(program), input_value=9)
    assert outputs[0] == 0


@pytest.mark.parametrize("program", [
    "3,9,7,9,10,9,4,9,99,-1,8",
    "3,3,1107,-1,8,3,4,3,99",
])
def test_less_than(program):
    _, outputs = process_memory(load_file(program), input_value=7)
    assert outputs[0] == 1

    _, outputs = process_memory(load_file(program), input_value=8)
    assert outputs[0] == 0

    _, outputs = process_memory(load_file(program), input_value=9)
    assert outputs[0] == 0


@pytest.mark.parametrize("input_value", [0, 1, 5, 6, 100])
@pytest.mark.parametrize("program", [
    "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9",
    "3,3,1105,-1,9,1101,0,0,12,4,12,99,1",
])
def test_jumps(input_value, program):
    _, outputs = process_memory(load_file(program), input_value=input_value)

    assert outputs[0] == int(input_value > 0)


@pytest.mark.parametrize(
    ["text", "memory", "outputs"], [["1002,4,3,4,33", "1002,4,3,4,99", []]]
)
def test_intcode(text, memory, outputs):
    m, o = process_memory(load_file(text))

    assert m == load_file(memory)
    assert o == outputs


if __name__ == "__main__":
    main_memory = load_file()

    main_memory, o = process_memory(main_memory, input_value=5)

    print(o)
