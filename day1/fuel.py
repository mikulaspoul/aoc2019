import math
import argparse

from pathlib import Path


def fuel_for_mass(mass):
    return math.floor(mass / 3) - 2


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--part-two", action="store_true", default=False)
    parser.add_argument("--file", default="input.txt")

    args = parser.parse_args()

    total = 0
    for line in Path(args.file).read_text().splitlines():
        x = int(line)
        # print(line, end=": ")
        while x := fuel_for_mass(x):
            if x <= 0:
                break
            # print(x, end=" ")
            total += x
            if not args.part_two:
                break
        # print("")
    print(total)
